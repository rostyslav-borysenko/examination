package edu.hneu.mjt.borysenko.ticket_1_20.dto;

import jakarta.validation.constraints.*;
import lombok.Data;

@Data
public class CuratorDto {
    private Integer id;

    @NotBlank(message = "Full name can not be empty")
    @NotNull(message = "Full name can not be NULL")
    @Size(max = 255, message = "Full name can not be longer than 255 letters")
    private String fullName;

    @NotBlank(message = "Specialty can not be empty")
    @NotNull(message = "Specialty can not be NULL")
    @Size(max = 255, message = "Specialty can not be longer than 255 characters")
    private String specialty;

    @NotNull(message = "Work experience can not be NULL")
    @Min(value = 0, message = "Work experience can not be less than 0")
    private int workExperience;

    @NotBlank(message = "Messenger can not be empty")
    @NotNull(message = "Messenger can not be NULL")
    @Pattern(regexp = "\\+38\\(0\\d{2}\\) \\d{3}-\\d{2}-\\d{2}",
            message = "Messenger must be in the format +38(0XX) XXX-XX-XX")
    private String messenger;

}
