package edu.hneu.mjt.borysenko.ticket_1_20.repository;

import edu.hneu.mjt.borysenko.ticket_1_20.entity.Exhibit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExhibitRepository extends JpaRepository<Exhibit, Integer> {
    @Query("SELECT c.fullName, COUNT(e) FROM Exhibit e JOIN e.curator c GROUP BY c.fullName")
    List<Object[]> getExhibitCountByCurator();
}
