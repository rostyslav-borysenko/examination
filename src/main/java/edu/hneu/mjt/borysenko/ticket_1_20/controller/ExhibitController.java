package edu.hneu.mjt.borysenko.ticket_1_20.controller;

import edu.hneu.mjt.borysenko.ticket_1_20.dto.ExhibitDto;
import edu.hneu.mjt.borysenko.ticket_1_20.entity.Exhibit;
import edu.hneu.mjt.borysenko.ticket_1_20.service.ExhibitService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/exhibits")
public class ExhibitController {
    @Autowired
    private ExhibitService exhibitService;

    private static final ModelMapper mapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<List<ExhibitDto>> getAllExhibits() {
        List<Exhibit> exhibits = exhibitService.findAll();
        List<ExhibitDto> exhibitDtos = exhibits.stream()
                .map((exhibit)-> mapper.map(exhibit, ExhibitDto.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(exhibitDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ExhibitDto> getExhibitById(@PathVariable Integer id) {
        Exhibit exhibit = exhibitService.findById(id);
        if (exhibit == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(mapper.map(exhibit, ExhibitDto.class));
    }

    @PostMapping
    public ResponseEntity<ExhibitDto> createExhibit(@RequestBody @Valid ExhibitDto exhibitDto) {
        System.out.println(exhibitDto);
        Exhibit exhibit = mapper.map(exhibitDto, Exhibit.class);
        exhibitService.save(exhibit);
        return ResponseEntity.ok(mapper.map(exhibit, ExhibitDto.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateExhibit(@PathVariable Integer id, @RequestBody @Valid ExhibitDto exhibitDto) {
        Exhibit existingExhibit = exhibitService.findById(id);
        if (existingExhibit == null) {
            return ResponseEntity.notFound().build();
        }

        Exhibit exhibit = mapper.map(exhibitDto, Exhibit.class);
        exhibit.setId(id);
        exhibitService.save(exhibit);
        return ResponseEntity.ok(mapper.map(exhibit, ExhibitDto.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteExhibit(@PathVariable Integer id) {
        exhibitService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/count-by-curator")
    public ResponseEntity<Map<String, Long>> getExhibitCountByCurator() {
        List<Object[]> results = exhibitService.getExhibitCountByCurator();
        Map<String, Long> countMap = results.stream()
                .collect(Collectors.toMap(
                        row->(String) row[0],
                        row->(Long) row[1]
                ));
        return ResponseEntity.ok(countMap);
    }
}
