package edu.hneu.mjt.borysenko.ticket_1_20.repository;

import edu.hneu.mjt.borysenko.ticket_1_20.entity.Curator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuratorRepository extends JpaRepository<Curator, Integer>  {
}