package edu.hneu.mjt.borysenko.ticket_1_20.dto;

import edu.hneu.mjt.borysenko.ticket_1_20.entity.Curator;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class ExhibitDto {
    @NotBlank(message = "Name can not be empty")
    @NotNull(message = "Name can not be NULL")
    @Size(max = 255, message = "Name can not be longer than 255 letters")
    private String name;

    @NotNull(message = "Year of creation can not be NULL")
    @Min(value = 1, message = "Work experience can not be less than 1")
    private int yearOfCreation;
    private String description;
    private Curator curator;
}
