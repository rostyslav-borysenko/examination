package edu.hneu.mjt.borysenko.ticket_1_20.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "exhibit")
public class Exhibit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "year_of_creation", nullable = false)
    private int yearOfCreation;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "curator_id", nullable = false)
    private Curator curator;
}