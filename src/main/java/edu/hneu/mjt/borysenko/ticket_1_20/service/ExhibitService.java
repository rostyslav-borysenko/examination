package edu.hneu.mjt.borysenko.ticket_1_20.service;

import edu.hneu.mjt.borysenko.ticket_1_20.entity.Exhibit;
import edu.hneu.mjt.borysenko.ticket_1_20.repository.ExhibitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ExhibitService {
    @Autowired
    private ExhibitRepository exhibitRepository;

    public void save(Exhibit exhibit) {
        exhibitRepository.save(exhibit);
    }

    public Exhibit findById(Integer id) {
        return exhibitRepository.findById(id).orElse(null);
    }

    public void deleteById(Integer id) {
        exhibitRepository.deleteById(id);
    }

    public List<Exhibit> findAll() {
        return exhibitRepository.findAll();
    }

    public List<Object[]> getExhibitCountByCurator() {
        return exhibitRepository.getExhibitCountByCurator();
    }
}
