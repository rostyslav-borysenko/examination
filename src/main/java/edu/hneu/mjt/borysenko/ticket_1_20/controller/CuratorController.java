package edu.hneu.mjt.borysenko.ticket_1_20.controller;

import edu.hneu.mjt.borysenko.ticket_1_20.dto.CuratorDto;
import edu.hneu.mjt.borysenko.ticket_1_20.entity.Curator;
import edu.hneu.mjt.borysenko.ticket_1_20.service.CuratorService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/curators")
public class CuratorController {
    @Autowired
    private CuratorService curatorService;

    private static final ModelMapper mapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<List<CuratorDto>> getAllCurators() {
        List<Curator> curators = curatorService.findAll();
        List<CuratorDto> curatorDTOs = curators.stream()
                .map((curator)-> mapper.map(curator, CuratorDto.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(curatorDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CuratorDto> getCuratorById(@PathVariable Integer id) {
        Curator curator = curatorService.findById(id);
        if (curator == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(mapper.map(curator, CuratorDto.class));
    }

    @PostMapping
    public ResponseEntity<CuratorDto> createCurator(@RequestBody @Valid CuratorDto curatorDto) {
        System.out.println(curatorDto);
        Curator curator = mapper.map(curatorDto, Curator.class);
        curatorService.save(curator);
        return ResponseEntity.ok(mapper.map(curator, CuratorDto.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCurator(@PathVariable Integer id, @RequestBody @Valid CuratorDto curatorDto) {
        Curator existingCurator = curatorService.findById(id);
        if (existingCurator == null) {
            return ResponseEntity.notFound().build();
        }

        Curator curator = mapper.map(curatorDto, Curator.class);
        curator.setId(id);
        curatorService.save(curator);
        return ResponseEntity.ok(mapper.map(curator, CuratorDto.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCurator(@PathVariable Integer id) {
        curatorService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
