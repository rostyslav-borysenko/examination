package edu.hneu.mjt.borysenko.ticket_1_20.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "curator")
public class Curator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "specialty", nullable = false)
    private String specialty;

    @Column(name = "work_experience", nullable = false)
    private int workExperience;

    @Column(name = "messenger", nullable = false)
    private String messenger;

    @OneToMany(mappedBy = "curator", cascade = CascadeType.ALL)
    private Set<Exhibit> exhibits = new HashSet<>();
}
