package edu.hneu.mjt.borysenko.ticket_1_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExaminationTicket120Application {

    public static void main(String[] args) {
        SpringApplication.run(ExaminationTicket120Application.class, args);
    }

}
