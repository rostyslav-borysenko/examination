package edu.hneu.mjt.borysenko.ticket_1_20.service;

import edu.hneu.mjt.borysenko.ticket_1_20.entity.Curator;
import edu.hneu.mjt.borysenko.ticket_1_20.repository.CuratorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CuratorService {
    @Autowired
    private CuratorRepository curatorRepository;

    public void save(Curator curator) {
        curatorRepository.save(curator);
    }

    public Curator findById(Integer id) {
        return curatorRepository.findById(id).orElse(null);
    }

    public void deleteById(Integer id) {
        curatorRepository.deleteById(id);
    }

    public List<Curator> findAll() {
        return curatorRepository.findAll();
    }
}
